/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : mu

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-12-25 23:06:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for muuser
-- ----------------------------
DROP TABLE IF EXISTS `muuser`;
CREATE TABLE `muuser` (
  `id` varchar(20) DEFAULT '',
  `username` varchar(50) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `birth` varchar(20) DEFAULT NULL,
  `from` varchar(50) DEFAULT NULL,
  `listen` varchar(30) DEFAULT NULL,
  `register` varchar(30) DEFAULT NULL,
  `lastLive` varchar(30) DEFAULT NULL,
  `userTeam` varchar(50) DEFAULT NULL,
  `help` varchar(20) DEFAULT NULL,
  `golden` varchar(20) DEFAULT NULL,
  `giveGolden` varchar(20) DEFAULT NULL,
  `safa` varchar(20) DEFAULT NULL,
  `topic` varchar(20) DEFAULT NULL,
  `online` varchar(30) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `redFlower` varchar(20) DEFAULT '',
  `head` varchar(200) DEFAULT NULL,
  KEY `idindex` (`id`),
  KEY `sexindex` (`sex`),
  KEY `fromindex` (`from`),
  KEY `birthindex` (`birth`),
  KEY `safaindex` (`safa`),
  KEY `redindex` (`redFlower`),
  KEY `registerindex` (`register`),
  KEY `onlineindex` (`online`),
  KEY `lastindex` (`lastLive`),
  KEY `topicindex` (`topic`),
  KEY `subjectindex` (`subject`),
  KEY `listenindex` (`listen`),
  KEY `goldindex` (`golden`),
  KEY `givegoldindex` (`giveGolden`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
