package com.muchong.util;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginUtil {
	/**
	 * 正则表达式匹配两个指定字符串中间的内容
	 * 
	 * @param soap
	 * @return
	 */
	static String regex = null;

	public static List<String> getSubUtil(String soap, String rgex) {
		List<String> list = new ArrayList<String>();
		Pattern pattern = Pattern.compile(rgex);// 匹配的模式
		Matcher m = pattern.matcher(soap);
		while (m.find()) {
			int i = 1;
			list.add(m.group(i));
			i++;
		}
		return list;
	}

	/**
	 * 返回单个字符串，若匹配到多个的话就返回第一个，方法与getSubUtil一样
	 * 
	 * @param soap
	 * @param rgex
	 * @return
	 */
	public static String getSubUtilSimple(String soap) {
		regex = "问题：(.*?)等于多少";
		Pattern pattern = Pattern.compile(regex);// 匹配的模式
		Matcher m = pattern.matcher(soap);
		while (m.find()) {
			return m.group(1);
		}
		return "";
	}

	static String calculate(String question) {
		String[] sss = new String[] {};

		sss = question.split("[\\u4e00-\\u9fa5]+");
		if (question.contains("加")) {
			return (Integer.parseInt(sss[0]) + Integer.parseInt(sss[1])) + "";
		}

		if (question.contains("减")) {
			return (Integer.parseInt(sss[0]) - Integer.parseInt(sss[1])) + "";
		}

		if (question.contains("乘以")) {
			return (Integer.parseInt(sss[0]) * Integer.parseInt(sss[1])) + "";
		}

		if (question.contains("除以")) {
			return (Integer.parseInt(sss[0]) / Integer.parseInt(sss[1])) + "";
		}

		return null;

	}
	/**
	 * 计算
	 * @param str
	 * @return
	 */
	public static String getResult(String str) {
		return calculate(getSubUtilSimple(str));
	}

	/**
	 * 测试
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		String str = "问题：46加1等于多少?\r\n" + "答案：";
		
		System.out.println(getResult(str));
	}

}
