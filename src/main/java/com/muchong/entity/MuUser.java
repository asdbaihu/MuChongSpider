package com.muchong.entity;



public class MuUser {
	// 用户名string
	// 听众 int
	// 注册时间
	// 最后活跃时间
	// 用户组string
	// 应助int
	// 金币int
	// 散金币int
	// 沙发int
	// 发帖int
	// 在线时间int
	// 专业string
	// 性别string
	// 来自string
	// 生日birth
	// 红花int
	// 头像string
	private String id;
	private String username;
	private String listen;
	private String register;
	private String lastLive;
	private String userTeam;
	private String help;
	private String golden;
	private String giveGolden;
	private String safa;
	private String topic;
	private String online;
	private String subject;
	private String sex;
	private String from;
	private String birth;
	private String redFlower;
	private String head;

	@Override
	public String toString() {
		return "MuUser [id=" + id + ", username=" + username + ", listen=" + listen + ", register=" + register
				+ ", lastLive=" + lastLive + ", userTeam=" + userTeam + ", help=" + help + ", golden=" + golden
				+ ", giveGolden=" + giveGolden + ", safa=" + safa + ", topic=" + topic + ", online=" + online
				+ ", subject=" + subject + ", sex=" + sex + ", from=" + from + ", birth=" + birth + ", redFlower="
				+ redFlower + ", head=" + head + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getListen() {
		return listen;
	}
	public void setListen(String listen) {
		this.listen = listen;
	}
	public String getRegister() {
		return register;
	}
	public void setRegister(String register) {
		this.register = register;
	}
	public String getLastLive() {
		return lastLive;
	}
	public void setLastLive(String lastLive) {
		this.lastLive = lastLive;
	}
	public String getUserTeam() {
		return userTeam;
	}
	public void setUserTeam(String userTeam) {
		this.userTeam = userTeam;
	}
	public String getHelp() {
		return help;
	}
	public void setHelp(String help) {
		this.help = help;
	}
	public String getGolden() {
		return golden;
	}
	public void setGolden(String golden) {
		this.golden = golden;
	}
	public String getGiveGolden() {
		return giveGolden;
	}
	public void setGiveGolden(String giveGolden) {
		this.giveGolden = giveGolden;
	}
	public String getSafa() {
		return safa;
	}
	public void setSafa(String safa) {
		this.safa = safa;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getOnline() {
		return online;
	}
	public void setOnline(String online) {
		this.online = online;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getRedFlower() {
		return redFlower;
	}
	public void setRedFlower(String redFlower) {
		this.redFlower = redFlower;
	}
	public String getHead() {
		return head;
	}
	public void setHead(String head) {
		this.head = head;
	}
	

}
